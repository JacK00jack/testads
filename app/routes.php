<?php
declare(strict_types=1);

use App\Application\Actions\VehicleMake\ListVehicleMakesAction;
use App\Application\Actions\VehicleMake\ViewVehicleMakesAction;
use App\Application\Actions\VehicleMake\CreateVehicleMakesAction;
use App\Application\Actions\VehicleMake\UpdateVehicleMakesAction;

use App\Application\Actions\VehicleModel\ListVehicleModelsAction;
use App\Application\Actions\VehicleModel\ViewVehicleModelsAction;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

use Tuupola\Middleware\JwtAuthentication;
use \Firebase\JWT\JWT;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello candidate! Good luck for the test and happy coding =P');
        return $response;
    });

    $app->group('/api', function (Group $group) {
        $group->get('/vehicle-makes', ListVehicleMakesAction::class);
        $group->get('/vehicle-makes/{id}', ViewVehicleMakesAction::class);
        $group->post('/vehicle-makes', CreateVehicleMakesAction::class);
        $group->post('/vehicle-makes/{id}', UpdateVehicleMakesAction::class)
          // ->add(JwtAuthentication::class);
          ->add(new JwtAuthentication([
            "secret" => "mF0vffPR%k8K5*y8mF0vffPR%k8K5*y8",
            'algorithm' => 'HS256',
            'secure' => false,
            "error" => function ($response, $arguments) {
                $data["status"] = 401;
                $data["message"] = 'Unauthorized/ '. $arguments["message"];
                return $response
                    ->withHeader("Content-Type", "application/json")
                    ->getBody()->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
            }
          ]));


        $group->get('/vehicle-models', ListVehicleModelsAction::class);
        $group->get('/vehicle-models/{id}', ViewVehicleModelsAction::class);
    });
};
