Postman collection inside /data/postman
































Automotive Data Solution - Backend skill test - PHP
==========================

## Tasks

The list of the tasks listed in the [Readme](../Readme.md) in the root directory of the bitbucket project.

## Requirement

* You need to have composer installed [Download composer](https://getcomposer.org/download/)

* You need to install docker desktop and Sign Up [Download Docker](https://www.docker.com/products/docker-desktop)


## Notes

* You are free to organize the content of the folder `src` as you want.
* You are free to install any packages

## How to run the project

### Install the application

```bash
cd php
composer install
```

### Run API

1) Spawn containers

```
docker-compose up
```

2) Check that everything is ok

```
curl http://localhost:8080
```

```
curl http://localhost:8080/api/vehicle-makes
```

Sample response

```
{
    "vehicle_makes": [
        {
            "id": 1,
            "name": "Acura",
            "url": "https://www.acura.ca"
        },
        {
            "id": 2,
            "name": "Alfa Romeo",
            "url": "https://www.alfaromeo.ca"
        }
    ]
}
```

### Interact with MySQL when you have to update the schema

1) Stop and Purge the MySQL container
```
./runmysql.sh  #Choose purge in the contextual menu
```

2) Re-run containers
```
docker-compose up
```

**IMPORTANT**: Everytime you modify the file `data\sql\update_database.sql` you have to run the script `runmysql.sh` with the command *purge*

Note: You can apply directly your SQL queries to test them in your favorite Mysql UI by using the credential defined in the `docker-compose.yml` file

### Unit Tests

```
composer test
```

### Documentation

This app use [PHP Slim Framework](https://www.slimframework.com/). This is a web framework written in PHP.
Complementary documentation can be found here: https://odan.github.io/2019/11/05/slim4-tutorial.html

We use docker compose to have a database MySQL in local accessible by this project.
The documentation of how to use *docker-compose* can be found here: https://docs.docker.com/compose/





### Database Structure

![Database structure](./data/img/gotest_mpd.png "Database Structure")


### Tasks

* TASK 1: As a user, I want to list all the vehicle model
    - Implement a new endpoint to do this action
* TASK 2: As a user, I want to add a vehicle make
    - Update the database migration script `data/sql/update_database.sql` add a column `created` in all the tables existing. (This column contain the date and the time when the data has been added)
    - Implement a new endpoint to do this action
* TASK 3: As a user, I want to update a vehicle make
    - Update the database migration script `data/sql/update_database.sql` add a column `updated` in all the tables existing. (This column contain the date and the time when the data has been changed)
    - Implement a new endpoint to do this action
* TASK 4: As a user, I want to be able to remove a vehicle make logically
    - Update the database migration script `data/sql/update_database.sql` add a column `state` in all the tables existing. This column can take 2 values 0/1. (0 means the entry has been deleted)
    - Implement a new endpoint to do this action

* TASK 5: Secure the call to insert / update / delete data with a JWT token

* TASK 6: As a user, I want to know all the vehicles available (means state=1) by make/year/model
    - Implement a new endpoint to do this action
* TASK 7: As a user, I want to be able to add a new vehicle
    - Update the database migration script `data/sql/update_database.sql` to avoid to have duplicate data for this couple (make,year,model)
    - Implement a new endpoint to do this action

* TASK 8: As a user, I want to be able to paginate the list of vehicle result
    - Implement a new endpoint to do this action

* TASK 9: Add some Unit Test (two at least)
    - Implement a new endpoint to do this action


Extra tasks:

* As a user, I want to add a vehicle model
* As a user, I want to update a vehicle model
* As a user, I want to delete a vehicle model logically
* As a user, I want the detail of a vehicle model


Note: At least provide us a list of curl or a postman collection (much appreciated) of all the endpoints you have created.


