<?php
declare(strict_types=1);

namespace App\Domain\VehicleModel;

use JsonSerializable;

class VehicleModel implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = 0;
        $this->name = '';
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param $id int
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param $name string
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param $data array
     */
    public function loadFromData(array $data): void
    {
        if (array_key_exists('vehicle_model_id', $data) && $data['vehicle_model_id'] != null) {
            $this->setId((int)$data['vehicle_model_id']);
        }

        if (array_key_exists('name', $data) && $data['name'] != null) {
            $this->setName($data['name']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
