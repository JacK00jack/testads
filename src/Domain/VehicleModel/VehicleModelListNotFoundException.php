<?php
declare(strict_types=1);

namespace App\Domain\VehicleModel;

use App\Domain\DomainException\DomainRecordNotFoundException;

class VehicleModelListNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The vehicle model list you requested does not exist.';
}
