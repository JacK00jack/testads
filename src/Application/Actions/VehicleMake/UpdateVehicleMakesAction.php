<?php
declare(strict_types=1);

namespace App\Application\Actions\VehicleMake;

use Psr\Http\Message\ResponseInterface as Response;

class UpdateVehicleMakesAction extends VehicleMakeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {

        $vehicleMakeId = (int) $this->resolveArg('id');
        $vehicleMake = $this->vehicleMakeRepository->findOne($vehicleMakeId);

        $vehicleMakeName = (string) $this->resolvePostBody('name');
        $vehicleMakeUrl = (string) $this->resolvePostBody('url');


        $result = $this->vehicleMakeRepository->update(
            $vehicleMakeId,
            $vehicleMakeName,
            $vehicleMakeUrl
        );

        $vehicleMake = $this->vehicleMakeRepository->findOne($vehicleMakeId);

        return $this->respondWithJSON($vehicleMake);



    }
}
