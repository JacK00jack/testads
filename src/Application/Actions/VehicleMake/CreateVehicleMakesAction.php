<?php
declare(strict_types=1);

namespace App\Application\Actions\VehicleMake;

use Psr\Http\Message\ResponseInterface as Response;

class CreateVehicleMakesAction extends VehicleMakeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {

        $vehicleMakeName = (string) $this->resolvePostBody('name');
        $vehicleMakeUrl = (string) $this->resolvePostBody('url');
        $vehicleMakeId = $this->vehicleMakeRepository->create(
            $vehicleMakeName,
            $vehicleMakeUrl
        );

        $vehicleMake = $this->vehicleMakeRepository->findOne($vehicleMakeId);

        return $this->respondWithJSON($vehicleMake);



    }
}
